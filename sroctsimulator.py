"""
    Python script for simulating the OCT signal for an ensemble of diffusing particles. If you use this code, please refer to
    
    Path-length-resolved diffusive particle dynamics in spectral-domain optical coherence tomography
    J. Kalkman, R. Sprik, and T. G. van Leeuwen, Physical Review Letters 105, 198302 (2010).
"""

import numpy as np
import matplotlib.pylab as plt

plt.close('all')

plt.figure(num=1, figsize=(6,8))

# Particle properties 
r=1e-7                              # particle radius [m]
eta=1e-3                            # viscosity [Pa s]
T=300                               # abolute temperature [K]
kB=1.38e-23                         # Boltzmann constant [J /K]
D=kB*T/(6*np.pi*eta*r)              # Stokes-Einstein equation 


# Molecular dynamics simulation
tau=1e-4                            # simulation time step
ntime=2048                          # number of time points
npart=1000                          # number of particles

# create time axis
time=np.linspace(0, ntime*tau, ntime)

# create random steps
steps=(2*(np.random.rand(npart, ntime)>0.5))-1

# calculate the displacements
xarray= np.sqrt(2*D*tau)*np.cumsum(steps, axis=1)

# add random zero time displacement
for cnt in range(npart):
    xarray[cnt,:]+=np.random.rand(1)

# calculate particle displacement
disp=xarray-np.matmul(np.expand_dims(xarray[:,0], axis=1),np.ones([1, ntime]))

plt.subplot(411)
plt.plot(time, np.mean((disp)**2, 0), '-b', label="Simulation")
plt.plot(time, 2*D*time, '--r', label="Theory")
plt.xlabel('Time (s)'), plt.ylabel('Mean square displacement (m)'), plt.legend(), plt.grid()

# OCT system parameters
nmed=1.33                           # refractive index of the medium
wavelength=900e-9                   # center wavelength
q=4*np.pi*nmed/wavelength           # scattering wavenumber (backscattering geometry)
rate=2*D*q**2                       # decorrelation rate
ks=2*np.pi/wavelength               # wavenumber
rate=1/D/q**2

# calculation of the particle average reflected field
field=np.zeros([1,ntime])
for k in range(npart):
    field=field+np.exp(1j*2*ks*nmed*xarray[k, :])
field=field/npart

# OCT signal is proportional to the real part of the field
signal=np.abs(field)-np.mean(np.abs(field),axis=1)

plt.subplot(412)
plt.plot(time, np.rot90(signal), '-b'), plt.xlabel('Time (s)'), plt.ylabel('Electric field (arb. units)')
plt.xlim([0, np.max(time)])

# calculate autocorrelation of field
acor = np.correlate(signal[0,:], signal[0,:], 'full')

# calculate normalization factor for unbiased estimation and normalize to unity
a=np.linspace(1, ntime-1, ntime-1)
b=np.linspace(ntime, 1, ntime)
norm=np.concatenate((a,b))
acor/=norm
acor/=acor[ntime-1]

timeacor=np.linspace(-ntime*tau, ntime*tau, 2*ntime-1)
modelac=np.exp(-2*np.abs(timeacor)*D*q**2)

plt.subplot(413)
plt.plot(timeacor, acor, '-b', timeacor, modelac, '--r')
plt.xlabel('Time (s)'), plt.ylabel('Autocorrelation'), plt.title('Autocorrelation')
plt.grid()
plt.xlim([-5*rate, 5*rate])
plt.ylim([-0.1, 1.2])

# calculate the frequency spectrum
ftsignal = np.fft.fftshift(np.fft.fft(signal, n=ntime, axis=1), axes=1)

# take the right hand side only
ftsignal = ftsignal[:,int(ntime/2):ntime+1]

# calculate the power spectral density
psd = np.abs(ftsignal)**2/((1/tau)*(ntime*tau))

# all power is in the positive frequency half
psd[:, int(ntime/2):ntime+1] *= 2

psdfreq=np.linspace(0, 1/tau/2, int(ntime/2))+1e-10
modelpsd=2*2*D*q**2/((2*D*q**2)**2 + (2*np.pi*psdfreq)**2)

plt.figure(1)
plt.subplot(414)
plt.loglog(psdfreq, np.mean(psd, axis=0), '-b')
plt.loglog(psdfreq, modelpsd, '--r')
plt.xlabel('Frequency (Hz)'), plt.ylabel('Power dB/Hz'), plt.title('Power spectral density')
plt.xlim([10, np.max(psdfreq)])
plt.ylim([1e-6, 1e-2]), plt.grid()

plt.tight_layout()