# SR-OCT simulator

This project provides a Python-based simulator of the spatially resolved OCT signal construction and analysis. 

The code was made with Python 3.7.3 and some basic output can be seen below. 

More information about the physics behind this can be found for diffusion [1] and with the extension to flow in [2].

[1] _Path-length-resolved diffusive particle dynamics in spectral-domain optical coherence tomography_, J. Kalkman, R. Sprik, and T. G. van Leeuwen, Physical Review Letters 105, 198302 (2010)

[2] _Localized measurement of longitudinal and transverse flow velocities in colloidal suspensions using optical coherence tomography_, N. Weiss, T.G. van Leeuwen, and J. Kalkman, Physical Review E 88, 042312 (2013)

<img src="sroctresults.png" alt="Output of the Python code" width="300"/>


